package org.shtarev.tmse14.commands;

import org.apache.log4j.PropertyConfigurator;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

public class AboutVersion extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return "AboutVersion";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show version this program";
    }

    @Override
    public void execute() {
        @NotNull final String log4jConfPath = "src/main/resources/log4j.properties";
        Enumeration<URL> resources = null;
        try {
            resources = ClassLoader.getSystemClassLoader().getResources("log4j.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert resources != null;
        PropertyConfigurator.configure(resources.nextElement());
        System.out.println("Task/Project manager Version:  1.0 ");
    }
}
