package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Project;

import java.util.List;

public class ProjectSort extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectSort";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort project by date";
    }

    @Override
    public void execute() {
        try {
            System.out.println("По какому признаку отсортировать список: ");
            System.out.println(" 1 - по дате начала проекта ");
            System.out.println(" 2 - по дате окончания проекта ");
            System.out.println(" 3 - по дате создания проекта ");
            System.out.println(" 4 - по статусу готовности проекта ");
            @NotNull final String sortedNumber = serviceLocator.getTerminalService().nextLine();
            List<Project> sortListProject = serviceLocator.getClientProjectService().projectsSort(sortedNumber, serviceLocator.getSession());
            System.out.println("Список отсортированных проектов:");
            sortListProject.forEach(project -> {
                        System.out.println("Статус:" + project.getTaskProjectStatus() +
                                " Название проекта: " + project.getName() + "  Описание проекта: " + project.getDescription() + "   ID проекта: " + project.getId());
                        System.out.println("                ID User: " + project.getUserId() + "   Дата начала проекта: " + project.getDataStart() + "  " +
                                " Дата окончания проекта: " + project.getDataFinish());
                        System.out.println("                Дата создания в ТМ:  " + project.getDataCreate());
                        System.out.println();
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
