package org.shtarev.tmse14.commands;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum  UserRole {
    @XmlEnumValue("0") ADMIN ,
    @XmlEnumValue("1") REGULAR_USER,
    @XmlEnumValue("2") NO_ROLE
}
