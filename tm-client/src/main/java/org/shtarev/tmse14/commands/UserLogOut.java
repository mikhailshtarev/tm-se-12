package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Session;
import org.shtarev.tmse14.controller.UserRole;

import java.util.ArrayList;
import java.util.List;

final public class UserLogOut extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserLogout";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User ends the session";
    }

    @Override
    public void execute() {
        Session session = new Session();
       serviceLocator.setSession(session);
        List<UserRole> userRole = new ArrayList<>();
       serviceLocator.setUserRole(userRole);
    }
}
