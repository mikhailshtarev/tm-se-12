package org.shtarev.tmse14.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.AbstractCommand;
import org.shtarev.tmse14.controller.Task;

final public class TaskCreateCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "TaskCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new Task";
    }

    @Override
    public void execute() {
        System.out.println("Введите название задачи:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание задачи:");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала задачи в формате uuuu-MM-dd : ");
        @NotNull final String dataStart = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания задачи в формате uuuu-MM-dd : ");
        @NotNull final String dataFinish = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите ID проекта, к которому относится задача: ");
        @NotNull final String projectID = serviceLocator.getTerminalService().nextLine();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setDataStart(dataStart);
        task.setDataFinish(dataFinish);
        task.setProjectId(projectID);
        if(serviceLocator.getClientTaskService().taskCreate(task, serviceLocator.getSession()))
            System.out.println("Задача создана!");
    }
}
