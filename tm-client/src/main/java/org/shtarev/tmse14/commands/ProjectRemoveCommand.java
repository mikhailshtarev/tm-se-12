package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;

final public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectRemove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected project by its ID";
    }


    @Override
    public void execute() {
        System.out.println("Введите ID проекта который хотите удалить:");
        @NotNull final String projectID = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getClientProjectService().projectDeleteById(projectID,serviceLocator.getSession());
    }
}
