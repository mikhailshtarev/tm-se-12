package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Project;
import org.shtarev.tmse14.controller.Task;

import java.util.List;

public class ProjectTaskFindByPart extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectTaskFindPart";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Find Task and Project by part Name or Description";
    }

    @Override
    public void execute() {
        System.out.println("Введите часть названия:  ");
        @NotNull final String findTP = serviceLocator.getTerminalService().nextLine();
        List<Project> sortProject = null;
        try {
            sortProject = serviceLocator.getClientProjectService().projectFindByDescription(findTP, serviceLocator.getSession());
            List<Task> sortTask = serviceLocator.getClientTaskService().taskFindByDescription(findTP, serviceLocator.getSession());
            System.out.println("Список отсортированных проектов и задач, которые в описании и названии содержат :  " + findTP);
            sortProject.forEach(project -> {
                        System.out.println("Статус:" + project.getTaskProjectStatus() +
                                " Название проекта: " + project.getName() + "  Описание проекта: " + project.getDescription() + "   ID проекта: " + project.getId());
                        System.out.println("                ID User: " + project.getUserId() + "   Дата начала проекта: " + project.getDataStart() + "  " +
                                " Дата окончания проекта: " + project.getDataFinish());
                        System.out.println("                Дата создания в ТМ:  " + project.getDataCreate());
                        System.out.println();
                    }
            );
            sortTask.forEach(task -> {
                        System.out.println("Статус:" + task.getTaskProjectStatus() +
                                " Название задачи: " + task.getName() + "  Описание задачи: " + task.getDescription() + "   ID проекта: " + task.getId());
                        System.out.println("                ID User: " + task.getUserId() + "   Дата начала проекта: " + task.getDataStart() + "  " +
                                " Дата окончания проекта: " + task.getDataFinish());
                        System.out.println("                Дата создания в ТМ:  " + task.getDataCreate());
                        System.out.println();
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
