package org.shtarev.tmse14.commands;


import org.jetbrains.annotations.NotNull;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create a new Project";
    }

    @Override
    public void execute() {
        System.out.println("Введите название проекта,который хотите создать: ");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание проекта: ");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала проекта в формате uuuu-MM-dd");
        @NotNull final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания проекта в формате uuuu-MM-dd");
        @NotNull final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        if(serviceLocator.getClientProjectService().projectCreate(projectName, description, dateStartSP, dateFinishSP,
                serviceLocator.getSession()))
            System.out.println("Проект создан!");;
    }
}
