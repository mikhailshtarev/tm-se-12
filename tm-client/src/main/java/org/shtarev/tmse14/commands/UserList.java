package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.User;

import java.util.List;


final public class UserList extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "View all Users";
    }

    @Override
    public void execute() {
        @NotNull final List<User> userNameList;
        try {
            userNameList = serviceLocator.getClientUserService().getUserList(serviceLocator.getSession());
            userNameList.forEach(user -> System.out.println("Имя пользователя: " + user.getName()
                    + "  ID пользователя: " + user.getUserId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
