package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;

final public class UserUpdate extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserUpdate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update user profile";
    }

    @Override
    public void execute() {
        System.out.println("Введите новое имя  пользователя:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getClientUserService().userUpdate(name, serviceLocator.getSession());
    }
}
