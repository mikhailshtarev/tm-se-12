package org.shtarev.tmse14.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.AbstractCommand;
import org.shtarev.tmse14.controller.Task;


public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "TaskUpdate";
    }

    @Override
    public String getDescription() {
        return "Update you Task";
    }

    @Override
    public void execute() {
        System.out.println("Введите ID задачи,которую хотите изменить: ");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новое описание задачи: ");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала задачи в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания задачи в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите статус задачи(PLANNED , IN_THE_PROCESS , READY)");
        @NotNull final String taskProjectStatus = serviceLocator.getTerminalService().nextLine();
        Task task = new Task();
        task.setDescription(description);
        task.setDataStart(dateStartSP);
        task.setDataFinish(dateFinishSP);
        serviceLocator.getClientTaskService().taskUpdate(taskId,task,taskProjectStatus,serviceLocator.getSession());
    }
}
