package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Session;

final public class UserLogIn extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserLogIn";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sign in";
    }

    @Override
    public void execute() {
        try {
            System.out.println("Введите имя пользователя: ");
            @NotNull final String name = serviceLocator.getTerminalService().nextLine();
            System.out.println("Введите пароль: ");
            @NotNull final String password = serviceLocator.getTerminalService().nextLine();
            if (name.equals("") || password.equals("")) {
                System.out.println("Одно из вводимых полей пустое");
                return;
            }
            Session session = serviceLocator.getClientUserService().LogIn(name, password);
            session.setHashBox(null);
            serviceLocator.setSession(session);
            serviceLocator.setUserRole(serviceLocator.getClientUserService().getUserRole(session));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Введен неправильный логин или пароль");
        }
    }
}
