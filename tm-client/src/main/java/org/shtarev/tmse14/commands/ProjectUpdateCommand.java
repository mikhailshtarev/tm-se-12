package org.shtarev.tmse14.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.AbstractCommand;
import org.shtarev.tmse14.controller.Project;

public class ProjectUpdateCommand extends AbstractCommand {
    @Override
    public String getName()  {
        return "ProjectUpdate";
    }

    @Override
    public String getDescription()  {
        return "Update you Project";
    }

    @Override
    public void execute() {
        System.out.println("Введите ID проекта,который хотите изменить: ");
        @NotNull final String projectId = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новое описание проекта: ");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала проекта в формате uuuu-MM-dd : ");
        @NotNull final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания проекта в формате uuuu-MM-dd : ");
        @NotNull final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите статус проета(PLANNED , IN_THE_PROCESS , READY)");
        @NotNull final String taskProjectStatus = serviceLocator.getTerminalService().nextLine();
        Project project = new Project();
        project.setDescription(description);
        project.setDataStart(dateStartSP);
        project.setDataFinish(dateFinishSP);
        serviceLocator.getClientProjectService().projectUpdate(projectId,project,taskProjectStatus,serviceLocator.getSession());
    }
}
