package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.User;

final public class UserCreate extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create New User";
    }


    @Override
    public void execute() {
        System.out.println("Введите имя нового пользователя: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль для нового пользователя: ");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (name.equals("") || password.equals("")) {
            System.out.println("Одно из вводимых полей пустое");
            return;
        }
        User thisUser = new User();
        thisUser.setName(name);
        thisUser.setPassword(password);
        if (serviceLocator.getClientUserService().create(thisUser))
            System.out.println("Новый пользователь создан!");
        else System.out.println("Пользователь с таким именем уже зарегистрирован");
    }
}
