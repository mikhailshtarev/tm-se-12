package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Task;

import java.util.List;


final public class TaskListCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "TaskList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all Tasks";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        @NotNull final List<Task> taskList;
        try {
            taskList = serviceLocator.getClientTaskService().taskList(serviceLocator.getSession());

        taskList.forEach(task -> {
            System.out.println("Статус:" + task.getTaskProjectStatus() +
                    "Название задачи: " + task.getName() + "  Описание задачи: " + task.getDescription());
            System.out.println("                ID задачи:  " + task.getId() + "  ID проекта к которому относится задача:  " + task.getProjectId());
            System.out.println("                Дата начала проекта: " + task.getDataStart() + "    Дата окончания проекта: " + task.getDataFinish());
            System.out.println("                Дата создания в ТМ: " + task.getDataCreate());
            System.out.println();
        });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}