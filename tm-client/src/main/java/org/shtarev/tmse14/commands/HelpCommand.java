package org.shtarev.tmse14.commands;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> lists = serviceLocator.getCommands();
        lists.forEach(abstractCommand -> System.out.println("Название команды: " + abstractCommand.getName() +
                "  Описание команды: " + abstractCommand.getDescription()));
    }
}
