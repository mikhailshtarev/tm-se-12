package org.shtarev.tmse14;

import com.sun.istack.Nullable;
import com.sun.istack.internal.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.shtarev.tmse14.commands.AbstractCommand;
import org.shtarev.tmse14.commands.ServiceLocator;
import org.shtarev.tmse14.controller.Session;
import org.shtarev.tmse14.controller.UserRole;
import org.shtarev.tmse14.service.*;

import java.util.*;

public class Bootstrap implements ServiceLocator {

    @NotNull
    @Getter
    @Setter
    public Session session;
    @Nullable
    @Getter
    @Setter
    public List<UserRole> userRole = new ArrayList<>();

    @Override
    public Scanner getTerminalService() {
        return terminalService;
    }

    @NotNull
    public final Scanner terminalService = new Scanner(System.in);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Getter
    private final ClientUserService clientUserService = new ClientUserServiceImpl();

    @NotNull
    @Getter
    private final ClientTaskService clientTaskService = new ClientTaskServiceImpl();

    @NotNull
    @Getter
    private final ClientProjectService clientProjectService = new ClientProjectServiceImpl();


    protected void init(Set<Class<?>> classes) {
        try {
            for (@NotNull final Class cl : classes) {
                Object o = cl.newInstance();
                if (o instanceof AbstractCommand) {
                    registry((AbstractCommand) o);
                } else throw new IllegalArgumentException("error");
            }
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    protected void registry(@NotNull final AbstractCommand command) {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (cliDescription == null || cliDescription.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }


    protected void start() {
        try {
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            String command = "";
            while (!"exit".equals(command)) {
                if (userRole.isEmpty())
                    userRole.add(0, UserRole.NO_ROLE);
                System.out.print(
                        "Role: " + userRole +
                                " |  Enter the terminal command: ");
                command = terminalService.nextLine();
                execute(command);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void execute(final String command) throws Exception {
        try {
            @NotNull final AbstractCommand abstractCommand = commands.get(command);
            abstractCommand.execute();
        } catch (NullPointerException e) {
            System.out.print(e.getMessage());
            System.out.println(" command entered incorrectly");
        }
    }
}
