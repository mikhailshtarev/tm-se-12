package org.shtarev.tmse14;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.shtarev.tmse14.commands.AbstractCommand;


import java.util.Collections;
import java.util.Set;

public class App {
    public static void main(String[] args) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("org.shtarev.tmse14.commands")).setScanners(new SubTypesScanner()));
        Set<Class<?>> classes;
        classes = Collections.unmodifiableSet(reflections.getSubTypesOf(AbstractCommand.class));
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(classes);
        bootstrap.start();



    }
}
