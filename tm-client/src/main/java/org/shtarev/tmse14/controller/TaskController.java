package org.shtarev.tmse14.controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.0
 * 2020-09-30T15:18:03.556+03:00
 * Generated source version: 3.4.0
 *
 */
@WebService(targetNamespace = "http://controller.tmse14.shtarev.org/", name = "TaskController")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskController {

    @WebMethod
    @Action(input = "http://controller.tmse14.shtarev.org/TaskController/taskDeleteAllRequest", output = "http://controller.tmse14.shtarev.org/TaskController/taskDeleteAllResponse")
    @RequestWrapper(localName = "taskDeleteAll", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskDeleteAll")
    @ResponseWrapper(localName = "taskDeleteAllResponse", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskDeleteAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean taskDeleteAll(

        @WebParam(name = "arg0", targetNamespace = "")
        org.shtarev.tmse14.controller.Session arg0
    );

    @WebMethod
    @Action(input = "http://controller.tmse14.shtarev.org/TaskController/taskReadAllRequest", output = "http://controller.tmse14.shtarev.org/TaskController/taskReadAllResponse", fault = {@FaultAction(className = IOException_Exception.class, value = "http://controller.tmse14.shtarev.org/TaskController/taskReadAll/Fault/IOException")})
    @RequestWrapper(localName = "taskReadAll", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskReadAll")
    @ResponseWrapper(localName = "taskReadAllResponse", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskReadAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<org.shtarev.tmse14.controller.Task> taskReadAll(

        @WebParam(name = "arg0", targetNamespace = "")
        org.shtarev.tmse14.controller.Session arg0
    ) throws IOException_Exception;

    @WebMethod
    @Action(input = "http://controller.tmse14.shtarev.org/TaskController/taskCreateRequest", output = "http://controller.tmse14.shtarev.org/TaskController/taskCreateResponse")
    @RequestWrapper(localName = "taskCreate", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskCreate")
    @ResponseWrapper(localName = "taskCreateResponse", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskCreateResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean taskCreate(

        @WebParam(name = "arg0", targetNamespace = "")
        org.shtarev.tmse14.controller.Task arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        org.shtarev.tmse14.controller.Session arg1
    );

    @WebMethod
    @Action(input = "http://controller.tmse14.shtarev.org/TaskController/taskFindByDescriptionRequest", output = "http://controller.tmse14.shtarev.org/TaskController/taskFindByDescriptionResponse", fault = {@FaultAction(className = IOException_Exception.class, value = "http://controller.tmse14.shtarev.org/TaskController/taskFindByDescription/Fault/IOException")})
    @RequestWrapper(localName = "taskFindByDescription", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskFindByDescription")
    @ResponseWrapper(localName = "taskFindByDescriptionResponse", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskFindByDescriptionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<org.shtarev.tmse14.controller.Task> taskFindByDescription(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        org.shtarev.tmse14.controller.Session arg1
    ) throws IOException_Exception;

    @WebMethod
    @Action(input = "http://controller.tmse14.shtarev.org/TaskController/taskDeleteRequest", output = "http://controller.tmse14.shtarev.org/TaskController/taskDeleteResponse")
    @RequestWrapper(localName = "taskDelete", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskDelete")
    @ResponseWrapper(localName = "taskDeleteResponse", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskDeleteResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean taskDelete(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        org.shtarev.tmse14.controller.Session arg1
    );

    @WebMethod
    @Action(input = "http://controller.tmse14.shtarev.org/TaskController/taskUpdateRequest", output = "http://controller.tmse14.shtarev.org/TaskController/taskUpdateResponse")
    @RequestWrapper(localName = "taskUpdate", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskUpdate")
    @ResponseWrapper(localName = "taskUpdateResponse", targetNamespace = "http://controller.tmse14.shtarev.org/", className = "org.shtarev.tmse14.controller.TaskUpdateResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean taskUpdate(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        org.shtarev.tmse14.controller.Task arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.String arg2,
        @WebParam(name = "arg3", targetNamespace = "")
        org.shtarev.tmse14.controller.Session arg3
    );
}
