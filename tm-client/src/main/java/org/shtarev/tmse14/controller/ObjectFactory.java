
package org.shtarev.tmse14.controller;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.shtarev.tmse14.controller package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IOException_QNAME = new QName("http://controller.tmse14.shtarev.org/", "IOException");
    private final static QName _GetUserList_QNAME = new QName("http://controller.tmse14.shtarev.org/", "getUserList");
    private final static QName _GetUserListResponse_QNAME = new QName("http://controller.tmse14.shtarev.org/", "getUserListResponse");
    private final static QName _GetUserRole_QNAME = new QName("http://controller.tmse14.shtarev.org/", "getUserRole");
    private final static QName _GetUserRoleResponse_QNAME = new QName("http://controller.tmse14.shtarev.org/", "getUserRoleResponse");
    private final static QName _Session_QNAME = new QName("http://controller.tmse14.shtarev.org/", "session");
    private final static QName _User_QNAME = new QName("http://controller.tmse14.shtarev.org/", "user");
    private final static QName _UserCreate_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userCreate");
    private final static QName _UserCreateResponse_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userCreateResponse");
    private final static QName _UserLogIn_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userLogIn");
    private final static QName _UserLogInResponse_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userLogInResponse");
    private final static QName _UserRePassword_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userRePassword");
    private final static QName _UserRePasswordResponse_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userRePasswordResponse");
    private final static QName _UserUpdate_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userUpdate");
    private final static QName _UserUpdateResponse_QNAME = new QName("http://controller.tmse14.shtarev.org/", "userUpdateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.shtarev.tmse14.controller
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link GetUserList }
     * 
     */
    public GetUserList createGetUserList() {
        return new GetUserList();
    }

    /**
     * Create an instance of {@link GetUserListResponse }
     * 
     */
    public GetUserListResponse createGetUserListResponse() {
        return new GetUserListResponse();
    }

    /**
     * Create an instance of {@link GetUserRole }
     * 
     */
    public GetUserRole createGetUserRole() {
        return new GetUserRole();
    }

    /**
     * Create an instance of {@link GetUserRoleResponse }
     * 
     */
    public GetUserRoleResponse createGetUserRoleResponse() {
        return new GetUserRoleResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link UserCreate }
     * 
     */
    public UserCreate createUserCreate() {
        return new UserCreate();
    }

    /**
     * Create an instance of {@link UserCreateResponse }
     * 
     */
    public UserCreateResponse createUserCreateResponse() {
        return new UserCreateResponse();
    }

    /**
     * Create an instance of {@link UserLogIn }
     * 
     */
    public UserLogIn createUserLogIn() {
        return new UserLogIn();
    }

    /**
     * Create an instance of {@link UserLogInResponse }
     * 
     */
    public UserLogInResponse createUserLogInResponse() {
        return new UserLogInResponse();
    }

    /**
     * Create an instance of {@link UserRePassword }
     * 
     */
    public UserRePassword createUserRePassword() {
        return new UserRePassword();
    }

    /**
     * Create an instance of {@link UserRePasswordResponse }
     * 
     */
    public UserRePasswordResponse createUserRePasswordResponse() {
        return new UserRePasswordResponse();
    }

    /**
     * Create an instance of {@link UserUpdate }
     * 
     */
    public UserUpdate createUserUpdate() {
        return new UserUpdate();
    }

    /**
     * Create an instance of {@link UserUpdateResponse }
     * 
     */
    public UserUpdateResponse createUserUpdateResponse() {
        return new UserUpdateResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserList }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "getUserList")
    public JAXBElement<GetUserList> createGetUserList(GetUserList value) {
        return new JAXBElement<GetUserList>(_GetUserList_QNAME, GetUserList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "getUserListResponse")
    public JAXBElement<GetUserListResponse> createGetUserListResponse(GetUserListResponse value) {
        return new JAXBElement<GetUserListResponse>(_GetUserListResponse_QNAME, GetUserListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserRole }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserRole }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "getUserRole")
    public JAXBElement<GetUserRole> createGetUserRole(GetUserRole value) {
        return new JAXBElement<GetUserRole>(_GetUserRole_QNAME, GetUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserRoleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetUserRoleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "getUserRoleResponse")
    public JAXBElement<GetUserRoleResponse> createGetUserRoleResponse(GetUserRoleResponse value) {
        return new JAXBElement<GetUserRoleResponse>(_GetUserRoleResponse_QNAME, GetUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Session }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Session }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "session")
    public JAXBElement<Session> createSession(Session value) {
        return new JAXBElement<Session>(_Session_QNAME, Session.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link User }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserCreate }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserCreate }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userCreate")
    public JAXBElement<UserCreate> createUserCreate(UserCreate value) {
        return new JAXBElement<UserCreate>(_UserCreate_QNAME, UserCreate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserCreateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserCreateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userCreateResponse")
    public JAXBElement<UserCreateResponse> createUserCreateResponse(UserCreateResponse value) {
        return new JAXBElement<UserCreateResponse>(_UserCreateResponse_QNAME, UserCreateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserLogIn }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserLogIn }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userLogIn")
    public JAXBElement<UserLogIn> createUserLogIn(UserLogIn value) {
        return new JAXBElement<UserLogIn>(_UserLogIn_QNAME, UserLogIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserLogInResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserLogInResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userLogInResponse")
    public JAXBElement<UserLogInResponse> createUserLogInResponse(UserLogInResponse value) {
        return new JAXBElement<UserLogInResponse>(_UserLogInResponse_QNAME, UserLogInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserRePassword }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserRePassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userRePassword")
    public JAXBElement<UserRePassword> createUserRePassword(UserRePassword value) {
        return new JAXBElement<UserRePassword>(_UserRePassword_QNAME, UserRePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserRePasswordResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserRePasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userRePasswordResponse")
    public JAXBElement<UserRePasswordResponse> createUserRePasswordResponse(UserRePasswordResponse value) {
        return new JAXBElement<UserRePasswordResponse>(_UserRePasswordResponse_QNAME, UserRePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserUpdate }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserUpdate }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userUpdate")
    public JAXBElement<UserUpdate> createUserUpdate(UserUpdate value) {
        return new JAXBElement<UserUpdate>(_UserUpdate_QNAME, UserUpdate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserUpdateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UserUpdateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://controller.tmse14.shtarev.org/", name = "userUpdateResponse")
    public JAXBElement<UserUpdateResponse> createUserUpdateResponse(UserUpdateResponse value) {
        return new JAXBElement<UserUpdateResponse>(_UserUpdateResponse_QNAME, UserUpdateResponse.class, null, value);
    }

}
