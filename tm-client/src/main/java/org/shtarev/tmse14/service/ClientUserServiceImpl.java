package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.*;

import java.util.List;

public class ClientUserServiceImpl implements ClientUserService {

    protected UserControllerImplService userControllerImplService = new UserControllerImplService();

    public UserController userControllerImplPort = userControllerImplService.getUserControllerImplPort();


    @Override
    public boolean create(@NotNull final User user) {
        return userControllerImplPort.userCreate(user);
    }

    @Override
    public List<User> getUserList(@NotNull final Session session) throws Exception {
        return userControllerImplPort.getUserList(session);
    }

    @Override
    public Session LogIn(@NotNull final String name, @NotNull final String password) throws Exception {
        if (userControllerImplPort.userLogIn(name, password) == null) {
            throw new Exception();
        }
        return userControllerImplPort.userLogIn(name, password);
    }

    @Override
    public boolean rePassword(@NotNull final String name, String oldPassword, @NotNull final String newPassword,
                              @NotNull final Session session) {
        return userControllerImplPort.userRePassword(name, oldPassword, newPassword, session);
    }

        @Override
    public boolean userUpdate(@NotNull final String name, @NotNull final Session session) {
        return userControllerImplPort.userUpdate(name, session);
    }

    @Override
    public List<UserRole> getUserRole(@NotNull final Session session) throws Exception {
        return userControllerImplPort.getUserRole(session);
    }

}
