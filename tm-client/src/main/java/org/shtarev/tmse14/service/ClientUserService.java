package org.shtarev.tmse14.service;


import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Session;
import org.shtarev.tmse14.controller.User;
import org.shtarev.tmse14.controller.UserRole;

import java.util.List;

public interface ClientUserService {


    boolean create(User user);

    List<User> getUserList(@NotNull Session session) throws Exception;

    Session LogIn(String name, String password) throws Exception;

    boolean rePassword(String name, String oldPassword, String newPassword, Session session);

    boolean userUpdate(@NotNull String name, @NotNull Session session);

    List<UserRole> getUserRole(Session session) throws Exception;
    //    boolean userUpdate(String name, Session session);
//
}
