package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Session;
import org.shtarev.tmse14.controller.Task;
import org.shtarev.tmse14.controller.TaskController;
import org.shtarev.tmse14.controller.TaskControllerImplService;

import java.util.List;

public class ClientTaskServiceImpl implements ClientTaskService {

    protected TaskControllerImplService taskControllerImplService = new TaskControllerImplService();

    public TaskController taskControllerImplPort = taskControllerImplService.getTaskControllerImplPort();

    @Override
    public boolean taskCreate(@NotNull final Task task, @NotNull final Session session) {
        return taskControllerImplPort.taskCreate(task, session);
    }

    @Override
    public @NotNull List<Task> taskList(@NotNull final Session session) throws Exception {
        return taskControllerImplPort.taskReadAll(session);
    }

    @Override
    public boolean taskUpdate(@NotNull final String taskId, @NotNull final Task task, @NotNull final String taskPS, @NotNull final Session session) {
        return taskControllerImplPort.taskUpdate(taskId, task, taskPS, session);
    }


    @Override
    public List<Task> taskFindByDescription(@NotNull final String partName, @NotNull final Session session) throws Exception {
        return taskControllerImplPort.taskFindByDescription(partName, session);
    }

    @Override
    public boolean taskDeleteAll(@NotNull final Session session) {
        return taskControllerImplPort.taskDeleteAll(session);
    }


    @Override
    public boolean taskDeleteOne(@NotNull final String taskId, @NotNull final Session session) {
        return taskControllerImplPort.taskDelete(taskId, session);
    }


}
