package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.controller.Project;
import org.shtarev.tmse14.controller.Session;

import java.util.List;

public interface ClientProjectService {

    boolean projectCreate(@NotNull String projectName, @NotNull String description,
                          @NotNull String dateStartSP, @NotNull String dateFinishSP,
                          @NotNull Session session);

    List<Project> projectReadAll(@NotNull Session session) throws Exception;

    List<Project> projectFindByDescription(@NotNull String partName, @NotNull Session session) throws Exception;

    List<Project> projectsSort(@NotNull String sort, @NotNull Session session)throws Exception;

    boolean projectUpdate(@NotNull String projectId, @NotNull Project project, @NotNull String taskProjectStatus, @NotNull Session session);

    boolean projectClear(Session session);

    boolean projectDeleteById(@NotNull String projectId, @NotNull Session session);


}
