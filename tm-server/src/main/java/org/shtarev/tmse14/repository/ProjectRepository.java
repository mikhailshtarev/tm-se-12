package org.shtarev.tmse14.repository;

import org.shtarev.tmse14.entity.Project;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ProjectRepository<T extends Project> {

    boolean create(T thisProject);

    Optional<List<Project>> getProjects(String userId) throws IOException;

    Optional<List<Project>> getProjectSotrDateStart(String userId) throws IOException;

    Optional<List<Project>> getProjectSotrDateFinish(String userId) throws IOException;

    Optional<List<Project>> getProjectSotrDateCreate(String userId) throws IOException;

    Optional<List<Project>> getProjectSotrStatus(String userId) throws IOException;

    boolean update( String projectId,  T thisProject);

    boolean remove(String Id, String userID);

    boolean removeAll(String userID);
}