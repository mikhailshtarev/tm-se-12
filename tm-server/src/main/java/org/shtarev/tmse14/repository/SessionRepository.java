package org.shtarev.tmse14.repository;

import org.shtarev.tmse14.entity.Session;

import java.util.Optional;

public interface SessionRepository {

    boolean createSes(Session session);

    Optional<Session> getSession(String sessionId);

    void deleteSession();


}
