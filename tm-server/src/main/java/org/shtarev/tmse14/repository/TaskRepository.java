package org.shtarev.tmse14.repository;

import org.shtarev.tmse14.entity.Task;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface TaskRepository<T extends Task> {

    boolean create(final T thisTask);

    Optional<List<Task>> getTasks(String userId) throws IOException;


    boolean update(final String taskId, final T thisProject);

    boolean remove(final String id, final String userID);

    boolean removeAll(final String userID);
}
