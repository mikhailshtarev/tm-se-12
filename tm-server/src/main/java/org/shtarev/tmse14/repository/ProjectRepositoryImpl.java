package org.shtarev.tmse14.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.StrInTPStatus;
import org.shtarev.tmse14.entity.Project;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


public class ProjectRepositoryImpl implements ProjectRepository<Project> {

    private final String PROJECTS_TABLE = "projects";
    private final String PROJECT_ID = "project_id";
    private final String PROJECT_NAME = "project_name";
    private final String PROJECT_DESCRIPTION = "project_description";
    private final String PROJECT_DATA_START = "data_start";
    private final String PROJECT_DATA_FINISH = "data_finish";
    private final String PROJECT_T_P_STATUS = "t_p_status";
    private final String PROJECT_DATA_CREATE = "data_create";
    private final String USER_ID = "user_id";

    private Connection getDbConnection()
            throws ClassNotFoundException, SQLException {
        final String dbPort = "127.0.0.1:3306";
        final String dbUser = "root";
        final String dbPass = "foxfox";
        final String dbName = "projecttaskbd";
        final String dbTimezone = "useUnicode=true&serverTimezone=UTC&useSSL=true&verifyServerCertificate=false";
        String connectionString = "jdbc:mysql://" + dbPort + "/" + dbName + "?" + dbTimezone;
        return DriverManager
                .getConnection(connectionString, dbUser, dbPass);
    }

    @SneakyThrows
    @Override
    public boolean create(@NotNull final Project thisProject) {
        String insert = "INSERT IGNORE INTO " + PROJECTS_TABLE + "(" +
                PROJECT_ID + "," + PROJECT_NAME + ","
                + PROJECT_DESCRIPTION + "," + PROJECT_DATA_START + ","
                + PROJECT_DATA_FINISH + "," + USER_ID + ","
                + PROJECT_T_P_STATUS + "," + PROJECT_DATA_CREATE + ")" +
                " VALUES(?,?,?,?,?,?,?,?)";
        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
            prST.setString(1, thisProject.getId());
            prST.setString(2, thisProject.getName());
            prST.setString(3, thisProject.getDescription());
            prST.setString(4, String.valueOf(thisProject.getDataStart()));
            prST.setString(5, String.valueOf(thisProject.getDataFinish()));
            prST.setString(6, thisProject.getUserId());
            prST.setString(7, Arrays.toString(thisProject.getTaskProjectStatus()));
            prST.setString(8, String.valueOf(thisProject.getDataCreate()));
            prST.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Optional<List<Project>> getProjects(@NotNull final String userId) throws IOException {
        ResultSet resSet;
        String select = "SELECT * FROM " + PROJECTS_TABLE + " WHERE " + USER_ID + "='" + userId + "'";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            List<Project> projectList = new ArrayList<>();
            resSet = prST.executeQuery();
            while (resSet.next()) {
                Project project = new Project();
                project.setId(resSet.getString(PROJECT_ID));
                project.setName(resSet.getString(PROJECT_NAME));
                project.setDescription(resSet.getString(PROJECT_DESCRIPTION));
                project.setDataStart(LocalDate.parse(resSet.getString(PROJECT_DATA_START)));
                project.setDataFinish(LocalDate.parse(resSet.getString(PROJECT_DATA_FINISH)));
                project.setUserId(resSet.getString(USER_ID));
                if (!StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).isPresent())
                    throw new IOException();
                project.setTaskProjectStatus(StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).get());
                project.setDataCreate(LocalDate.parse(resSet.getString(PROJECT_DATA_CREATE)));
                projectList.add(project);
            }
            return Optional.of(projectList);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }

    @Override
    public Optional<List<Project>> getProjectSotrDateStart(String userId) throws IOException {
        ResultSet resSet;
        String select = "SELECT * FROM " + PROJECTS_TABLE + " WHERE " + USER_ID + "='" + userId + "' ORDER BY " + PROJECT_DATA_START;
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            List<Project> projectList = new ArrayList<>();
            resSet = prST.executeQuery();
            while (resSet.next()) {
                Project project = new Project();
                project.setId(resSet.getString(PROJECT_ID));
                project.setName(resSet.getString(PROJECT_NAME));
                project.setDescription(resSet.getString(PROJECT_DESCRIPTION));
                project.setDataStart(LocalDate.parse(resSet.getString(PROJECT_DATA_START)));
                project.setDataFinish(LocalDate.parse(resSet.getString(PROJECT_DATA_FINISH)));
                project.setUserId(resSet.getString(USER_ID));
                if (!StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).isPresent())
                    throw new IOException();
                project.setTaskProjectStatus(StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).get());
                project.setDataCreate(LocalDate.parse(resSet.getString(PROJECT_DATA_CREATE)));
                projectList.add(project);
            }
            return Optional.of(projectList);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }

    @Override
    public Optional<List<Project>> getProjectSotrDateFinish(String userId) throws IOException {
        ResultSet resSet;
        String select = "SELECT * FROM " + PROJECTS_TABLE + " WHERE " + USER_ID + "='" + userId + "' ORDER BY " + PROJECT_DATA_FINISH;
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            List<Project> projectList = new ArrayList<>();
            resSet = prST.executeQuery();
            while (resSet.next()) {
                Project project = new Project();
                project.setId(resSet.getString(PROJECT_ID));
                project.setName(resSet.getString(PROJECT_NAME));
                project.setDescription(resSet.getString(PROJECT_DESCRIPTION));
                project.setDataStart(LocalDate.parse(resSet.getString(PROJECT_DATA_START)));
                project.setDataFinish(LocalDate.parse(resSet.getString(PROJECT_DATA_FINISH)));
                project.setUserId(resSet.getString(USER_ID));
                if (!StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).isPresent())
                    throw new IOException();
                project.setTaskProjectStatus(StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).get());
                project.setDataCreate(LocalDate.parse(resSet.getString(PROJECT_DATA_CREATE)));
                projectList.add(project);
            }
            return Optional.of(projectList);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }

    @Override
    public Optional<List<Project>> getProjectSotrDateCreate(String userId) throws IOException {
        ResultSet resSet;
        String select = "SELECT * FROM " + PROJECTS_TABLE + " WHERE " + USER_ID + "='" + userId + "' ORDER BY " + PROJECT_DATA_CREATE;
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            List<Project> projectList = new ArrayList<>();
            resSet = prST.executeQuery();
            while (resSet.next()) {
                Project project = new Project();
                project.setId(resSet.getString(PROJECT_ID));
                project.setName(resSet.getString(PROJECT_NAME));
                project.setDescription(resSet.getString(PROJECT_DESCRIPTION));
                project.setDataStart(LocalDate.parse(resSet.getString(PROJECT_DATA_START)));
                project.setDataFinish(LocalDate.parse(resSet.getString(PROJECT_DATA_FINISH)));
                project.setUserId(resSet.getString(USER_ID));
                if (!StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).isPresent())
                    throw new IOException();
                project.setTaskProjectStatus(StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).get());
                project.setDataCreate(LocalDate.parse(resSet.getString(PROJECT_DATA_CREATE)));
                projectList.add(project);
            }
            return Optional.of(projectList);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }

    @Override
    public Optional<List<Project>> getProjectSotrStatus(String userId) throws IOException {
        ResultSet resSet;
        String select = "SELECT * FROM " + PROJECTS_TABLE + " WHERE " + USER_ID + "='" + userId + "' ORDER BY " + PROJECT_T_P_STATUS;
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            List<Project> projectList = new ArrayList<>();
            resSet = prST.executeQuery();
            while (resSet.next()) {
                Project project = new Project();
                project.setId(resSet.getString(PROJECT_ID));
                project.setName(resSet.getString(PROJECT_NAME));
                project.setDescription(resSet.getString(PROJECT_DESCRIPTION));
                project.setDataStart(LocalDate.parse(resSet.getString(PROJECT_DATA_START)));
                project.setDataFinish(LocalDate.parse(resSet.getString(PROJECT_DATA_FINISH)));
                project.setUserId(resSet.getString(USER_ID));
                if (!StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).isPresent())
                    throw new IOException();
                project.setTaskProjectStatus(StrInTPStatus.strInTPStatus(resSet.getString(PROJECT_T_P_STATUS)).get());
                project.setDataCreate(LocalDate.parse(resSet.getString(PROJECT_DATA_CREATE)));
                projectList.add(project);
            }
            return Optional.of(projectList);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }


    @Override
    public boolean update(@NotNull final String projectId, @NotNull final Project thisProject) {
        String select = "UPDATE " + PROJECTS_TABLE + " SET " + PROJECT_DESCRIPTION + "='" + thisProject.getDescription()
                + "'," + PROJECT_DATA_START + "='" + thisProject.getDataStart() + "'," + PROJECT_DATA_FINISH + "='"
                + thisProject.getDataFinish() + "'," + PROJECT_T_P_STATUS + "='"
                + Arrays.toString(thisProject.getTaskProjectStatus()) + "' WHERE "
                + PROJECT_ID + "='" + projectId + "'";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            prST.executeUpdate();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean remove(@NotNull final String id, @NotNull final String userID) {
        String insert = "DELETE FROM " + PROJECTS_TABLE + " WHERE "
                + PROJECT_ID + "='" + id + "';";
        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
            prST.executeUpdate();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public boolean removeAll(@NotNull final String userID) {
        String insert = "DELETE FROM " + PROJECTS_TABLE + " WHERE "
                + USER_ID + "='" + userID + "';";
        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
            prST.executeUpdate();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}

