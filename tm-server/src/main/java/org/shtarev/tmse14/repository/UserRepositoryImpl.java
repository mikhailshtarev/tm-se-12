package org.shtarev.tmse14.repository;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


public class UserRepositoryImpl implements UserRepository<User> {
    private final String USER_ID = "user_id";
    private final String USER_NAME = "user_name";
    private final String USER_PASSWORD = "user_password";
    private final String USER_ROLE = "user_role";
    private final String USER_TABLE = "users";
    private final String USER_ROLE_TABLE = "userrole";
    private final String USER_ID_ROLE_ID_TABLE = "user_id_role_id";
    private final String USER_ROLE_ID = "role_id";
    private String userRoleId;

    private Connection getDbConnection()
            throws ClassNotFoundException, SQLException {
        final String dbPort = "127.0.0.1:3306";
        final String dbName = "projecttaskbd";
        final String dbTimezone = "useUnicode=true&serverTimezone=UTC&useSSL=true&verifyServerCertificate=false";
        final String dbUser = "root";
        final String dbPass = "foxfox";
        String connectionString = "jdbc:mysql://" + dbPort + "/" + dbName + "?" + dbTimezone;
        return DriverManager
                .getConnection(connectionString, dbUser, dbPass);
    }


    @Override
    public boolean create(@NotNull final User thisUser) throws SQLException {
        String insert1 = "INSERT IGNORE INTO " + USER_TABLE + "(" +
                USER_ID + "," + USER_NAME + ","
                + USER_PASSWORD + ")" +
                "VALUES(?,?,?)";
        String insert2 = "INSERT IGNORE INTO " + USER_ID_ROLE_ID_TABLE + "(" +
                USER_ID + "," + USER_ROLE_ID + ")" + "VALUES(?,?)";
        String select1;
        select1 = "SELECT " + USER_ROLE_ID + " FROM " + USER_ROLE_TABLE + " WHERE " + USER_ROLE + " = '"
                + Objects.requireNonNull(thisUser.getUserRole()).get(0).toString() + "';";
        String select0 = "SELECT * FROM " + USER_TABLE + " WHERE " + USER_NAME + " = '"
                + thisUser.getName() + "';";
        Connection conn = null;
        try {
            conn = getDbConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement prST2 = getDbConnection().prepareStatement(select1);
            ResultSet resSet = prST2.executeQuery();
            if (resSet.next()) {
                userRoleId = resSet.getString(USER_ROLE_ID);
            }
            PreparedStatement prST4 = getDbConnection().prepareStatement(select0);
            ResultSet resSet4 = prST4.executeQuery();
            if (resSet4.next()) {
                if (thisUser.getName() != null && thisUser.getName().equals(resSet4.getString(USER_NAME))) return false;
            }
            Objects.requireNonNull(conn).setAutoCommit(false);
            PreparedStatement prST = conn.prepareStatement(insert1);
            prST.setString(1, thisUser.getUserId());
            prST.setString(2, thisUser.getName());
            prST.setString(3, thisUser.getPassword());
            prST.execute();
            PreparedStatement prST3 = conn.prepareStatement(insert2);
            prST3.setString(1, thisUser.getUserId());
            prST3.setString(2, userRoleId);
            prST3.execute();
            conn.commit();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Objects.requireNonNull(conn).rollback();
            return false;
        } finally {
            Objects.requireNonNull(conn).setAutoCommit(true);
            conn.close();
        }

    }


    @Override
    public Optional<User> getUser(@NotNull final String userName) {
        ResultSet resSet;
        String select = "SELECT " + USER_TABLE + "." + USER_ID + "," + USER_NAME + "," + USER_PASSWORD + "," + USER_ROLE + " FROM "
                + USER_TABLE + " JOIN " + USER_ID_ROLE_ID_TABLE + " ON " + USER_ID_ROLE_ID_TABLE + "." + USER_ID + "="
                + USER_TABLE + "." + USER_ID + " JOIN " + USER_ROLE_TABLE + " ON " + USER_ROLE_TABLE + "." + USER_ROLE_ID + "="
                + USER_ID_ROLE_ID_TABLE + "." + USER_ROLE_ID + " WHERE " + USER_TABLE + "." + USER_NAME + "='" + userName + "'";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            resSet = prST.executeQuery();
            User user = new User();
            if (resSet.next()) {
                user.setName(resSet.getString(USER_NAME));
                user.setPassword(resSet.getString(USER_PASSWORD));
                user.setUserId(resSet.getString(USER_ID));
                String userRole = resSet.getString(USER_ROLE);
                if (userRole.equals("ADMIN")) {
                    List<UserRole> userRole1 = new ArrayList<>();
                    userRole1.add(0, UserRole.ADMIN);
                    user.setUserRole(userRole1);
                }
                if (userRole.equals("REGULAR_USER")) {
                    List<UserRole> userRole1 = new ArrayList<>();
                    userRole1.add(0, UserRole.REGULAR_USER);
                    user.setUserRole(userRole1);

                }
                return Optional.of(user);
            }

        } catch (
                SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }


    @Override
    public Optional<List<User>> getUserList() {
        ResultSet resSet;
        String select = "SELECT * FROM " + USER_TABLE;
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            List<User> userlist = new ArrayList<>();
            resSet = prST.executeQuery();
            while (resSet.next()) {
                User user = new User();
                user.setUserId(resSet.getString(1));
                user.setPassword(resSet.getString(2));
                String userRole = resSet.getString(3);
                if (userRole.equals("ADMIN")) {
                    List<UserRole> userRole1 = new ArrayList<>();
                    userRole1.add(0, UserRole.ADMIN);
                    user.setUserRole(userRole1);
                }
                if (userRole.equals("REGULAR_USER")) {
                    List<UserRole> userRole1 = new ArrayList<>();
                    userRole1.add(0, UserRole.REGULAR_USER);
                    user.setUserRole(userRole1);
                }
                userlist.add(user);
            }
            return Optional.of(userlist);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();

        }
        return Optional.empty();
    }


    @Override
    public boolean rePassword(@NotNull final String userName, @NotNull final String oldPasswordHash,
                              @NotNull final String newPasswordHash) {
        String select = "UPDATE " + USER_TABLE + " SET " + USER_PASSWORD + "='" + newPasswordHash + "' WHERE " +
                USER_PASSWORD + "='" + oldPasswordHash + "'";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            prST.executeUpdate();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateUser(@NotNull final String newName, @NotNull final String thisUserName) {
        String select = "UPDATE " + USER_TABLE + " SET " + USER_NAME + "='" + newName + "' WHERE " +
                USER_NAME + "='" + thisUserName + "'";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            prST.executeUpdate();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Optional<List<UserRole>> getListUserRole(String userName) {
        ResultSet resSet;
        String select = "SELECT " + USER_TABLE + "." + USER_ID + "," + USER_NAME + "," + USER_PASSWORD + "," + USER_ROLE + " FROM "
                + USER_TABLE + " JOIN " + USER_ID_ROLE_ID_TABLE + " ON " + USER_ID_ROLE_ID_TABLE + "." + USER_ID + "="
                + USER_TABLE + "." + USER_ID + " JOIN " + USER_ROLE_TABLE + " ON " + USER_ROLE_TABLE + "." + USER_ROLE_ID + "="
                + USER_ID_ROLE_ID_TABLE + "." + USER_ROLE_ID + " WHERE " + USER_TABLE + "." + USER_NAME + "='" + userName + "'";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            resSet = prST.executeQuery();
            if (resSet.next()) {
                String userRole = resSet.getString(USER_ROLE);
                if (userRole.equals("ADMIN")) {
                    List<UserRole> userRole1 = new ArrayList<>();
                    userRole1.add(0, UserRole.ADMIN);
                    return Optional.of(userRole1);
                }
                if (userRole.equals("REGULAR_USER")) {
                    List<UserRole> userRole2 = new ArrayList<>();
                    userRole2.add(0, UserRole.REGULAR_USER);
                    return Optional.of(userRole2);
                }
            }

        } catch (
                SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
