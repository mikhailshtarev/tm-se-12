package org.shtarev.tmse14.repository;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Session;

import java.sql.*;
import java.util.Optional;

public class SessionRepositoryImpl implements SessionRepository {

    private final String SESSIONS_TABLE = "sessions";
    private final String SESSION_ID = "session_id";
    private final String SESSION_DATA_CREATE = "data_create";
    private final String SESSION_HASH_BOX = "hash_box";
    private final String USER_NAME = "user_name";
    private final String USER_ID = "user_id";

    private Connection getDbConnection()
            throws ClassNotFoundException, SQLException {
        final String dbPort = "127.0.0.1:3306";
        final String dbUser = "root";
        final String dbPass = "foxfox";
        final String dbName = "projecttaskbd";
        final String dbTimezone = "useUnicode=true&serverTimezone=UTC&useSSL=true&verifyServerCertificate=false";
        String connectionString = "jdbc:mysql://" + dbPort + "/" + dbName + "?" + dbTimezone;
        return DriverManager
                .getConnection(connectionString, dbUser, dbPass);
    }


    @Override
    public boolean createSes(@NotNull final Session session) {
        String insert = "INSERT INTO " + SESSIONS_TABLE + "(" + SESSION_ID + "," +
                USER_NAME + "," + SESSION_DATA_CREATE + "," + SESSION_HASH_BOX + ")" +
                " VALUES(?,?,?,?)";
        try (PreparedStatement prST = getDbConnection().prepareStatement(insert)) {
            prST.setString(1, session.getId());
            prST.setString(2, session.getUserName());
            prST.setDate(3, Date.valueOf((session.getDataCreate())));
            prST.setString(4, session.getHashBox());
            prST.execute();
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

    }


    @Override
    public Optional<Session> getSession(@NotNull final String sessionUserName) {
        ResultSet resSet = null;
        String select = "SELECT * FROM " + SESSIONS_TABLE + " WHERE "
                + USER_NAME + "=?";
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            prST.setString(1, sessionUserName);
            resSet = prST.executeQuery();
            if (resSet.next()) {
                Session session = new Session();
                session.setId(resSet.getString(SESSION_ID));
                session.setUserName(resSet.getString(USER_NAME));
                session.setDataCreate(resSet.getDate(SESSION_DATA_CREATE).toLocalDate());
                session.setHashBox(resSet.getString(SESSION_HASH_BOX));
                return Optional.of(session);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public void deleteSession() {
        String select = "DELETE FROM " + SESSIONS_TABLE + " WHERE datediff(now()," + SESSION_DATA_CREATE+")>=1" ;
        try (PreparedStatement prST = getDbConnection().prepareStatement(select)) {
            prST.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
