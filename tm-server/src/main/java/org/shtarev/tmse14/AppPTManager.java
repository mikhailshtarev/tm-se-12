package org.shtarev.tmse14;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.controller.ProjectControllerImpl;
import org.shtarev.tmse14.controller.TaskControllerImpl;

import org.shtarev.tmse14.controller.UserControllerImpl;
import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.entity.Task;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.*;
import org.shtarev.tmse14.service.*;

import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AppPTManager {
    @NotNull
    private final UserRepository<User> userRepository = new UserRepositoryImpl();
    @NotNull
    private final SessionRepository sessionRepository = new SessionRepositoryImpl();
    @NotNull
    private final TaskRepository<Task> taskRepository = new TaskRepositoryImpl();
    @NotNull
    private final ProjectRepository<Project> projectRepository = new ProjectRepositoryImpl();
    @NotNull
    private final SessionUtil sessionUtil = new SessionUtilImpl(sessionRepository);
    @NotNull
    private final ProjectService<Project> projectService = new ProjectServiceImpl(projectRepository, taskRepository, userRepository, sessionUtil);
    @NotNull
    private final UserService<User> userService = new UserServiceImpl(userRepository, sessionUtil);
    @NotNull
    private final TaskService<Task> taskService = new TaskServiceImpl(taskRepository, userRepository, sessionUtil);

    @SneakyThrows
    public static void main(String[] args) {
        AppPTManager appPTManager = new AppPTManager();
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        Runnable sesDel = appPTManager::sessionDeleteByTime;
        ScheduledFuture<?> scheduledFuture = ses.scheduleAtFixedRate(sesDel, 5, 3600, TimeUnit.SECONDS);
        appPTManager.createTwoUser();
        System.setProperty("javax.xml.transform.TransformerFactory", "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
        Endpoint.publish("http://0.0.0.0:8080/ProjectEndopoint", new ProjectControllerImpl(appPTManager.projectService));
        Endpoint.publish("http://0.0.0.0:8080/TaskEndopoint", new TaskControllerImpl(appPTManager.taskService));
        Endpoint.publish("http://0.0.0.0:8080/UserEndopoint", new UserControllerImpl(appPTManager.userService));
    }

    public void createTwoUser() {
        List<UserRole> listUR = new ArrayList<>(Collections.singleton(UserRole.REGULAR_USER));
        List<UserRole> listUR2 = new ArrayList<>(Collections.singleton(UserRole.ADMIN));
        User user1 = new User();
        User user2 = new User();
        user1.setName("ad");
        user1.setPassword("ad");
        user1.setUserRole(listUR2);
        user2.setName("ru");
        user2.setPassword("ru");
        user2.setUserRole(listUR);
        userService.create(user1);
        userService.create(user2);
    }

    public void sessionDeleteByTime(){
        sessionUtil.deleteSession();
    }
}


