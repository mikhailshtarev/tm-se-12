package org.shtarev.tmse14.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class HashProperties {

    public static String getSalt() {
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try {
            fileInputStream = new FileInputStream("C:\\projects\\tm-se-11\\tm-server\\src\\main\\resources\\hash.properties");
            prop.load(fileInputStream);
            return prop.getProperty("salo");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Integer getCycol() {
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try {
            fileInputStream = new FileInputStream("C:\\projects\\tm-se-11\\tm-server\\src\\main\\resources\\hash.properties");
            prop.load(fileInputStream);
            return Integer.valueOf(prop.getProperty("cicol"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
