package org.shtarev.tmse14.commands;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum TaskProjectStatus {
    @XmlEnumValue("PLANNED") PLANNED,
    @XmlEnumValue("IN_THE_PROCESS") IN_THE_PROCESS,
    @XmlEnumValue("READY") READY
}
