package org.shtarev.tmse14.commands;

import com.sun.istack.internal.NotNull;

import java.util.List;

public class UserRoleInRoleId {

    public static String userRoleInRoleId(@NotNull final List<UserRole> listRole) {
        StringBuffer userRoleId = new StringBuffer("");
        if (listRole.contains(UserRole.ADMIN))
            userRoleId.insert(0, 1);
        if (listRole.contains(UserRole.REGULAR_USER))
            userRoleId.insert(0, 2);
        if (listRole.contains(UserRole.NO_ROLE))
            userRoleId.insert(0, 3);
        return String.valueOf(userRoleId);
    }
}
