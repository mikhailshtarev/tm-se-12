package org.shtarev.tmse14.commands;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum UserRole {
    @XmlEnumValue("ADMIN") ADMIN ,
    @XmlEnumValue("REGULAR_USER") REGULAR_USER,
    @XmlEnumValue("NO_ROLE") NO_ROLE

}
