package org.shtarev.tmse14.controller;

import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@WebService
public interface UserController {

    @WebMethod
    boolean userCreate(User user);

    @WebMethod
    Session userLogIn(String name, String password) throws IOException;

    @WebMethod
    List<User> getUserList(Session session)throws IOException;


    @WebMethod
    boolean userRePassword(String name, String oldPassword, String newPassword, Session session);

    @WebMethod
    boolean userUpdate(String name, Session session);

    @WebMethod
    List<UserRole> getUserRole(Session session) throws IOException ;


}
