package org.shtarev.tmse14.controller;

import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@WebService
public interface TaskController {

    @WebMethod
    boolean taskCreate(Task task, Session session);

    @WebMethod
    List<Task> taskReadAll(Session session) throws IOException;

    @WebMethod
    List<Task> taskFindByDescription(String partName, Session session) throws IOException;

    @WebMethod
    boolean taskUpdate(String taskId, Task task, String taskPS, Session session);

    @WebMethod
    boolean taskDelete(String id, Session session);

    @WebMethod
    boolean taskDeleteAll(Session session);
}
