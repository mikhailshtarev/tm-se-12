package org.shtarev.tmse14.controller;

import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@WebService
public interface ProjectController {


    @WebMethod
    boolean projectCreate(Project project, Session session);

    @WebMethod
    List<Project> projectsReadAll(Session session) throws IOException;

    @WebMethod
    List<Project> projectFindByDescription(String partName, Session session) throws IOException;

    @WebMethod
    List<Project> projectSorted(String sort, Session session)throws IOException;

    @WebMethod
    boolean projectUpdate(String projectId, Project project, String taskProjectStatus, Session session);

    @WebMethod
    boolean projectDelete(String id, Session session);


    @WebMethod
    boolean projectDeleteAll(Session session);
}
