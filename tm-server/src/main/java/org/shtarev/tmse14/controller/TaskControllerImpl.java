package org.shtarev.tmse14.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.Task;
import org.shtarev.tmse14.service.TaskService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebService(endpointInterface = "org.shtarev.tmse14.controller.TaskController")
public class TaskControllerImpl implements TaskController {

    @NotNull
    private final TaskService<Task> taskService;

    public TaskControllerImpl(@NotNull final TaskService<Task> taskService) {
        this.taskService = taskService;
    }

    @Override
    public boolean taskCreate(@NotNull final Task task, @NotNull final Session session) {
        return taskService.create(task, session);
    }

    @Override
    public List<Task> taskReadAll(@NotNull final Session session) throws IOException {
        Optional<List<Task>> listTask = taskService.taskReadAll(session);
        if (listTask.isPresent())
            return listTask.get();
        else throw new IOException();
    }

    @Override
    public List<Task> taskFindByDescription(@NotNull final String partName, @NotNull final Session session) throws IOException {
        Optional<List<Task>> listTask = taskService.findByDescription(partName, session);
        if (listTask.isPresent())
            return listTask.get();
        else throw new IOException();
    }

    @Override
    public boolean taskUpdate(@NotNull final String taskId, @NotNull final Task task,
                              @NotNull final String taskPS, @NotNull final Session session) {
        return taskService.update(taskId, task, taskPS, session);
    }

    @Override
    public boolean taskDelete(@NotNull final String id, @NotNull final Session session) {
        return taskService.delete(id, session);
    }


    @Override
    public boolean taskDeleteAll(@NotNull final Session session) {
        return taskService.taskDeleteAll(session);
    }
}
