package org.shtarev.tmse14.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.service.UserService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@WebService(endpointInterface = "org.shtarev.tmse14.controller.UserController")
public class UserControllerImpl implements UserController {

    @NotNull
    private final UserService<User> userService;

    public UserControllerImpl(@NotNull final UserService<User> userService) {
        this.userService = userService;
    }

    @Override
    public boolean userCreate(@NotNull final User user) {
        List<UserRole> listUR = new ArrayList<>(Collections.singleton(UserRole.REGULAR_USER));
        user.setUserRole(listUR);
        return userService.create(user);
    }

    @Override
    public List<User> getUserList(@NotNull final Session session) throws IOException {
        Optional<List<User>> listUser = userService.getUserList(session);
        if (listUser.isPresent())
            return listUser.get();
        else throw new IOException();
    }

    @Override
    public Session userLogIn(@NotNull final String name, @NotNull final String password) throws IOException {
        return userService.LogIn(name, password);
    }

    @Override
    public boolean userRePassword(@NotNull final String name, @NotNull final String oldPassword,
                                  @NotNull final String newPassword, @NotNull final Session session) {
        return userService.rePassword(name, oldPassword, newPassword, session);
    }


    @Override
    public boolean userUpdate(@NotNull final String name, @NotNull final Session session) {
        return userService.userUpdate(name, session);
    }

    @Override
    public List<UserRole> getUserRole(@NotNull final Session session) throws IOException {
        Optional<List<UserRole>> listUserRole = userService.getListUserRole(session);
        if (listUserRole.isPresent())
            return listUserRole.get();
        else throw new IOException();
    }
}
