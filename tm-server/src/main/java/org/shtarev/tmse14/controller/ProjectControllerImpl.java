package org.shtarev.tmse14.controller;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.service.ProjectService;

import javax.jws.WebService;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebService(endpointInterface = "org.shtarev.tmse14.controller.ProjectController")
public class ProjectControllerImpl implements ProjectController {


    private final @NotNull ProjectService<Project> projectService;

    public ProjectControllerImpl(final @NotNull ProjectService<Project> projectService) {
        this.projectService = projectService;
    }

    @Override
    public boolean projectCreate(@NotNull final Project project, @NotNull final Session session) {
        return projectService.create(project, session);
    }

    @Override
    public List<Project> projectsReadAll(@NotNull final Session session) throws IOException {
        Optional<List<Project>> listProject = projectService.projectsReadAll(session);
        if (listProject.isPresent())
            return listProject.get();
        else throw new IOException();
    }

    @Override
    public List<Project> projectFindByDescription(@NotNull final String partName, @NotNull final Session session) throws IOException {
        Optional<List<Project>> listProject = projectService.findByDescription(partName, session);
        if (listProject.isPresent())
            return listProject.get();
        else throw new IOException();
    }

    @Override
    public List<Project> projectSorted(@NotNull final String sort, @NotNull final Session session) throws IOException {
        Optional<List<Project>> listProject = projectService.sorted(sort, session);
        if (listProject.isPresent())
            return listProject.get();
        else throw new IOException();
    }

    @Override
    public boolean projectUpdate(@NotNull final String projectId, @NotNull final Project project,
                                 @NotNull final String taskProjectStatus, @NotNull final Session session) {
        try {
            return projectService.update(projectId, project, taskProjectStatus, session);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean projectDelete(@NotNull final String projectId, @NotNull final Session session) {
        try {
            return projectService.delete(projectId, session);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }


    @Override
    public boolean projectDeleteAll(@NotNull final Session session) {
        return projectService.projectDeleteAll(session);
    }
}
