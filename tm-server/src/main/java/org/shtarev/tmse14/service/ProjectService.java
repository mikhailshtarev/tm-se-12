package org.shtarev.tmse14.service;

import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.entity.Session;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


public interface ProjectService<T extends Project> {


    boolean create(Project project, Session session);

    Optional<List<T>> projectsReadAll(Session session) throws IOException;

    Optional<List<T>> findByDescription(String partName, Session session) throws IOException;

    Optional<List<T>> sorted(String sort, Session session)throws IOException;

    boolean update(String projectId, Project project,String taskProjectStatus, Session session) throws IOException;



    boolean delete(String id, Session session) throws IOException;


    boolean projectDeleteAll(Session session);
}
