package org.shtarev.tmse14.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.TaskProjectStatus;
import org.shtarev.tmse14.entity.Project;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.Task;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.ProjectRepository;
import org.shtarev.tmse14.repository.TaskRepository;
import org.shtarev.tmse14.repository.UserRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.shtarev.tmse14.commands.UserRole.ADMIN;
import static org.shtarev.tmse14.commands.UserRole.REGULAR_USER;

@Slf4j
public class ProjectServiceImpl implements ProjectService<Project> {
    @NotNull
    private final ProjectRepository<Project> projectRepository;
    @NotNull
    private final TaskRepository<Task> taskRepository;
    @NotNull
    private final UserRepository<User> userRepository;
    @NotNull
    private final SessionUtil sessionUtil;

    @NotNull
    public ProjectServiceImpl(@NotNull ProjectRepository<Project> projectRepository, @NotNull TaskRepository<Task> taskRepository,
                              @NotNull UserRepository<User> userRepository,
                              @NotNull SessionUtil sessionUtil) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.sessionUtil = sessionUtil;
    }

    @Override
    public boolean create(@NotNull final Project project, @NotNull final Session session) {
        try {
            if (sessionUtil.checkSession(session)) {
                @NotNull User user;
                @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    project.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.PLANNED});
                    project.setUserId(user.getUserId());
                    return projectRepository.create(project);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    @Nullable
    public Optional<List<Project>> projectsReadAll(@NotNull final Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
            if (userOpt.isPresent())
                user = userOpt.get();
            else throw new IOException();
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                return projectRepository.getProjects(user.getUserId());
            }
        }
        throw new IOException();
    }

    @Override
    public Optional<List<Project>> findByDescription(@NotNull final String partName, @NotNull final Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
            if (userOpt.isPresent())
                user = userOpt.get();
            else throw new IOException();
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                Optional<List<Project>> projectOpt = projectRepository.getProjects(user.getUserId());
                if (projectOpt.isPresent()) {
                    List<Project> projectList = projectOpt.get();
                    List<Project> projectReturn = new ArrayList<>();
                    projectList.forEach(project -> {
                        if (project.getName() != null && project.getDescription() != null &&
                                (project.getDescription().contains(partName) || project.getName().contains(partName)))
                            projectReturn.add(project);
                    });
                    return Optional.of(projectReturn);
                }
            }
        }
        return Optional.empty();
    }


    @Override
    public Optional<List<Project>> sorted(@NotNull final String sort, @NotNull final Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
            if (userOpt.isPresent())
                user = userOpt.get();
            else throw new IOException();
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                switch (sort) {
                    case "1":
                        Optional<List<Project>> listProject = projectRepository.getProjectSotrDateStart(user.getUserId());
                        if (listProject.isPresent())
                            return listProject;
                        else throw new IOException();
                    case "2":
                        Optional<List<Project>> listProject2 = projectRepository.getProjectSotrDateFinish(user.getUserId());
                        if (listProject2.isPresent())
                            return listProject2;
                        else throw new IOException();
                    case "3":
                        Optional<List<Project>> listProject3 = projectRepository.getProjectSotrDateCreate(user.getUserId());
                        if (listProject3.isPresent())
                            return listProject3;
                        else throw new IOException();
                    case "4":
                        Optional<List<Project>> listProject4 = projectRepository.getProjectSotrStatus(user.getUserId());
                        if (listProject4.isPresent())
                            return listProject4;
                        else throw new IOException();
                    default:
                        return Optional.empty();
                }

            }
        }
        return Optional.empty();
    }

    @Override
    public boolean update(@NotNull final String projectId, @NotNull final Project project,
                          @NotNull final String taskProjectStatus, @NotNull final Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
            if (userOpt.isPresent())
                user = userOpt.get();
            else return false;
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                if (taskProjectStatus.equals("IN_THE_PROCESS"))
                    project.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.IN_THE_PROCESS});
                if (taskProjectStatus.equals("READY"))
                    project.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.READY});
                return projectRepository.update(projectId, project);

            }
        }
        return false;
    }

    @Override
    public boolean delete(@NotNull final String id, @NotNull final Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            if (id.isEmpty()) {
                return false;
            }
            @NotNull User user;
            @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
            if (userOpt.isPresent())
                user = userOpt.get();
            else return false;
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                projectRepository.remove(id, session.getUserName());
                taskRepository.remove(id, session.getUserName());
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean projectDeleteAll(@NotNull final Session session) {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            try {
                @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    taskRepository.removeAll(user.getUserId());
                    projectRepository.removeAll(user.getUserId());
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}

