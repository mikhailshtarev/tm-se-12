package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.TaskProjectStatus;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.Task;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.TaskRepository;
import org.shtarev.tmse14.repository.UserRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.shtarev.tmse14.commands.UserRole.ADMIN;
import static org.shtarev.tmse14.commands.UserRole.REGULAR_USER;

public class TaskServiceImpl implements TaskService<Task> {
    @NotNull
    private final TaskRepository<Task> taskRepository;

    @NotNull
    private final UserRepository<User> userRepository;

    @NotNull
    private final SessionUtil sessionUtil;

    @NotNull
    public TaskServiceImpl(final @NotNull TaskRepository<Task> taskRepository, @NotNull final UserRepository<User> userRepository,
                           @NotNull final SessionUtil sessionUtil) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.sessionUtil = sessionUtil;

    }

    @Override
    public boolean create(@NotNull final Task task, @NotNull final Session session) {
        try {
            if (sessionUtil.checkSession(session)) {
                @NotNull final User user;
                Optional<User> optUser = userRepository.getUser(session.getUserName());
                if (optUser.isPresent())
                    user = optUser.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    task.setUserId(user.getUserId());
                    task.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.PLANNED});
                    return taskRepository.create(task);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    @Nullable
    public Optional<List<Task>> taskReadAll(@NotNull final Session session) throws IOException {

        if (sessionUtil.checkSession(session)) {

            @NotNull final User user;
            Optional<User> optUser = userRepository.getUser(session.getUserName());
            if (optUser.isPresent())
                user = optUser.get();
            else throw new IOException();
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                return taskRepository.getTasks(user.getUserId());
            }
        }
        throw new IOException();
    }

    @Override
    public boolean update(@NotNull final String taskId, @NotNull final Task task, @NotNull final String taskPS,
                          @NotNull final Session session) {
        if (sessionUtil.checkSession(session)) {
            try {
                User user;
                Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    if (taskPS.equals("IN_THE_PROCESS"))
                        task.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.IN_THE_PROCESS});
                    if (taskPS.equals("READY"))
                        task.setTaskProjectStatus(new TaskProjectStatus[]{TaskProjectStatus.READY});
                    return taskRepository.update(taskId, task);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public Optional<List<Task>> findByDescription(@NotNull final String partName, @NotNull final Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            @NotNull final Optional<User> userOpt = userRepository.getUser(session.getUserName());
            if (userOpt.isPresent())
                user = userOpt.get();
            else throw new IOException();
            if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                Optional<List<Task>> taskOpt = taskRepository.getTasks(user.getUserId());
                if (taskOpt.isPresent()) {
                    List<Task> taskList = taskOpt.get();
                    List<Task> taskReturn = new ArrayList<>();
                    taskList.forEach(task -> {
                        if (task.getName() != null && task.getDescription() != null &&
                                (task.getDescription().contains(partName) || task.getName().contains(partName)))
                            taskReturn.add(task);
                    });
                    return Optional.of(taskReturn);
                }
            }
        }
        return Optional.empty();
    }


    @Override
    public boolean delete(@NotNull final String id, @NotNull final Session session) {
        try {
            if (sessionUtil.checkSession(session)) {
                if (id.isEmpty()) {
                    return false;
                }
                @NotNull final User user;
                Optional<User> optUser = userRepository.getUser(session.getUserName());
                if (optUser.isPresent())
                    user = optUser.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN)
                        || user.getUserRole().contains(REGULAR_USER))) {
                    taskRepository.remove(id, session.getUserName());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean taskDeleteAll(@NotNull final Session session) {
        if (sessionUtil.checkSession(session)) {
            try {
                User user;
                Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    taskRepository.removeAll(session.getUserName());
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}





