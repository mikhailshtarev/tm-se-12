package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Session;

import java.util.Optional;

public interface SessionUtil {

    Optional<Session> createSession(@NotNull String UserName);

    boolean checkSession(@NotNull Session session);

    void deleteSession();

    Optional<Session> checkSessionByName(String name);
}
