package org.shtarev.tmse14.service;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.repository.SessionRepository;
import org.shtarev.tmse14.util.HashProperties;
import org.shtarev.tmse14.util.SignatureUtil;

import java.time.LocalDate;
import java.util.Optional;

public class SessionUtilImpl implements SessionUtil {

    private final SessionRepository sessionRepository;

    public SessionUtilImpl(@NotNull final SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public Optional<Session> createSession(@NotNull final String userName) {
        Session session = new Session();
        session.setDataCreate(LocalDate.now());
        session.setUserName(userName);
        session.setHashBox(SignatureUtil.sign(session, HashProperties.getSalt(), HashProperties.getCycol()));
        if (sessionRepository.createSes(session))
            return Optional.of(session);
        else return Optional.empty();

    }

    public boolean checkSession(@NotNull final Session session) {
        Optional<Session> session1 = sessionRepository.getSession(session.getUserName());
        session.setHashBox(SignatureUtil.sign(session, HashProperties.getSalt(), HashProperties.getCycol()));
        return session1.get().getHashBox().equals(session.getHashBox());
    }

    @Override
    public void deleteSession() {
        sessionRepository.deleteSession();
    }

    @Override
    public Optional<Session> checkSessionByName(String name) {
        return sessionRepository.getSession(name);
    }
}
