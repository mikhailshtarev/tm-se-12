package org.shtarev.tmse14.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.User;
import org.shtarev.tmse14.repository.UserRepository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.shtarev.tmse14.commands.UserRole.ADMIN;
import static org.shtarev.tmse14.commands.UserRole.REGULAR_USER;

@Slf4j
public class UserServiceImpl implements UserService<User> {

    @NotNull
    private final UserRepository<User> userRepository;


    @NotNull
    private final SessionUtil sessionUtil;

    @NotNull
    public UserServiceImpl(@NotNull final UserRepository<User> userRepository, @NotNull final SessionUtil sessionUtil) {
        this.userRepository = userRepository;
        this.sessionUtil = sessionUtil;
    }

    public boolean create(@NotNull final User user) {
        if (user.getName() == null || user.getUserRole() == null || user.getPassword() == null)
            return false;
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        try {
            return userRepository.create(user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    @Override
    @Nullable
    public Session LogIn(@NotNull final String name, @NotNull final String password) throws IOException {
        Optional<User> userOpt = userRepository.getUser(name);
        if (userOpt.isPresent()) {
            @NotNull final User thisUser = userOpt.get();
            @NotNull final String thisPassword = DigestUtils.md5Hex(password);
            if (name.equals(thisUser.getName()) && thisPassword.equals(thisUser.getPassword())) {
                Optional<Session> sessionOpt = sessionUtil.checkSessionByName(name);
                return sessionOpt.orElseGet(() -> sessionUtil.createSession(thisUser.getName()).get());
            }
        }
        throw new IOException();

    }

    @Nullable
    public Optional<List<User>> getUserList(@NotNull final Session session) {
        if (sessionUtil.checkSession(session)) {
            @NotNull User user;
            try {
                Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return Optional.empty();
                if (user.getUserRole() != null && (user.getUserRole()).contains(ADMIN)) {
                    return userRepository.getUserList();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Optional.empty();
    }


    @Override
    public boolean rePassword(@NotNull String name, @NotNull String oldPassword, @NotNull String newPassword,
                              @NotNull final Session session) {
        if (sessionUtil.checkSession(session)) {
            if (name.isEmpty() || oldPassword.isEmpty() || newPassword.isEmpty()) {
                return false;
            }
            try {
                @NotNull User user;
                Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    @NotNull final String oldPassword2 = DigestUtils.md5Hex(oldPassword);
                    @NotNull final String newPassword2 = DigestUtils.md5Hex(newPassword);
                    return userRepository.rePassword(name, oldPassword2, newPassword2);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public boolean userUpdate(@NotNull final String newName, @NotNull final Session session) {
        if (sessionUtil.checkSession(session)) {
            if (newName.isEmpty() || session.getUserName().isEmpty()) {
                return false;
            }
            try {
                User user;
                Optional<User> userOpt = userRepository.getUser(session.getUserName());
                if (userOpt.isPresent())
                    user = userOpt.get();
                else return false;
                if (user.getUserRole() != null && (user.getUserRole().contains(ADMIN) || user.getUserRole().contains(REGULAR_USER))) {
                    return userRepository.updateUser(newName, session.getUserName());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public Optional<List<UserRole>> getListUserRole(Session session) throws IOException {
        if (sessionUtil.checkSession(session)) {
            return userRepository.getListUserRole(session.getUserName());
        }
        throw new IOException();
    }
}


