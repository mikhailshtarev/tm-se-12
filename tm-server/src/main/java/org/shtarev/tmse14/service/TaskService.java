package org.shtarev.tmse14.service;

import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.Task;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface TaskService<T extends Task> {

    boolean create(T task, Session session);

    Optional<List<T>> taskReadAll(Session session) throws IOException;

    boolean update(String taskId, T task, String taskPS, Session session);

    Optional<List<T>> findByDescription(String partName, Session session) throws IOException;

    boolean delete(String id, Session session);

    boolean taskDeleteAll(Session session);
}
