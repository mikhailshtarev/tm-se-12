package org.shtarev.tmse14.service;

import org.shtarev.tmse14.commands.UserRole;
import org.shtarev.tmse14.entity.Session;
import org.shtarev.tmse14.entity.User;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService<U extends User> {

    boolean create(U user);

    Optional<List<U>> getUserList(Session session) throws IOException;

    Session LogIn(String name, String password) throws IOException;

    boolean rePassword(String name, String oldPassword, String newPassword, Session session);

    boolean userUpdate(String name, Session session);

    Optional<List<UserRole>> getListUserRole(Session session) throws IOException;
}