package org.shtarev.tmse14.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.UserRole;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class User implements Serializable {


    @Nullable
    private String name;

    @Nullable
    private String password;

    @Nullable
    private List<UserRole> userRole;

    @NotNull
    private String userId = UUID.randomUUID().toString();


}
