package org.shtarev.tmse14.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.shtarev.tmse14.util.LocalDateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.UUID;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Getter
@Setter
public class Session {

    private String id = UUID.randomUUID().toString();

    private String userName;

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "uuuu-MM-dd")
    private LocalDate dataCreate;

    private String hashBox;

}
