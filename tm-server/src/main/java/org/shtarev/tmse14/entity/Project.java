package org.shtarev.tmse14.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse14.commands.TaskProjectStatus;
import org.shtarev.tmse14.util.LocalDateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;


@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull
    public String id = UUID.randomUUID().toString();

    @Nullable
    private String name;


    @Nullable
    private String description;


    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
    private LocalDate dataStart;


    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
    private LocalDate dataFinish;


    @Nullable
    private String userId;


    @Nullable
    private TaskProjectStatus[] taskProjectStatus;


    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
    private LocalDate dataCreate = LocalDate.now();
}
